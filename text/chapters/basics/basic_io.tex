\subsubsection{Теория}

\par Для того, чтобы быстро передать какое-то сообщение от программы и получить ответ пользователя, удобно использовать \inlvba{MsgBox} или \inlvba{InputBox}.
\inlvba{MsgBox} выводит окно с сообщением и кнопками.
В зависимости от того, какую кнопку нажмёт пользователь, \inlvba{Msgbox} передаст в программу разные значения.
\inlvba{InputBox} выводит окно со строкой ввода и кнопкой OK и передаёт программе текст, который пользователь ввёл в поле.

\par Остановимся подробнее на \inlvba{MsgBox}.
Он выглядит следующим образом (рис.~\ref{fig:bas_msgbox_01}):
\begin{figure}[H]
 \centering
 \includegraphics{pictures/msgbox_01.PNG}
 \caption{обычное сообщение}
 \label{fig:bas_msgbox_01}
\end{figure}

\par Это сообщение создаётся при запуске вот такой процедуры:
\lstinputlisting[style=vba_listing]{code/basics/msgbox_example_01.tex}
\par Разберёмся, какая часть команды за что отвечает. У \inlvba{MsgBox} в данном случае использованы три аргумента.
Первый -- текст сообщения.
Второй -- описание формы, в которой показывается сообщение.
\inlvba{vbInformation} означает, что сообщение будет показываться со значком ``i в кружочке''.
Третий аргумент -- заголовок формы, в которой показывается сообщение.
Если бы мы не указали его самостоятельно, то в качестве заголовка использовалось бы название приложения (в данном случае это Excel).

\par Кроме значка, мы можем также поменять кнопки, которые есть на форме.
Вся настройка внешнего вида делается с помощью второго аргумента, в котором указывается сумма констант для нужных нам доработок.
Например, вместо \inlvba{vbInformation} мы могли бы написать \inlvba{64}, и результат был бы тот же.
В таблице~\ref{tab:bas_msgbox_constants} приводится список некоторых констант, которыми мы можем пользоваться для изменения вида \inlvba{MsgBox}.
Они разделены на три блока.
Из каждого блока можно выбрать по одной константе и записать их сумму во второй аргумент \inlvba{MsgBox}.
Первый блок добавляет кнопки, второй -- значок, а третий блок определяет, какая по счёту кнопка должна быть активной при вызове \inlvba{MsgBox}.

\begin{longtable}{lll}
    
    \hline\hline
    Constant & Value & Description  \\
    \hline
    \endhead
   
    vbOKOnly & 0 & Display OK button only. \\
    vbOKCancel  & 1 & Display OK and Cancel buttons. \\
    vbAbortRetryIgnore  & 2 & Display Abort, Retry, and Ignore buttons. \\
    vbYesNoCancel  & 3 & Display Yes, No, and Cancel buttons. \\
    vbYesNo  & 4 & Display Yes and No buttons. \\
    vbRetryCancel  & 5 & Display Retry and Cancel buttons. \\
    \hline
    vbCritical  & 16 & Display Critical Message icon. \\
    vbQuestion  & 32 & Display Warning Query icon. \\
    vbExclamation  & 48 & Display Warning Message icon. \\
    vbInformation  & 64 & Display Information Message icon. \\
    \hline
    vbDefaultButton1  & 0 & First button is default. \\
    vbDefaultButton2  & 256 & Second button is default. \\ 
    vbDefaultButton3  & 512 & Third button is default. \\
    vbDefaultButton4  & 768 & Fourth button is default. \\
   \hline
   \caption{константы для Message Box}
   \label{tab:bas_msgbox_constants}
\end{longtable} 

\par Текст, который мы хотим показать пользователю, можно передать в \inlvba{MsgBox} внутри какой-нибудь переменной или вычислить на месте.
То, как это делать, можно посмотреть в процедуре \inlvba{msgbox_example_02}. 

\lstinputlisting[style=vba_listing]{code/basics/msgbox_example_02.tex}

\par Например, \inlvba{MsgBox} на десятой строке (рис.~\ref{fig:bas_msgbox_02}) получает сообщение, которое ему надо отобразить, через переменную \inlvba{message}.
А настройка кнопок и значка делается с помощью суммы констант \inlvba{vbYesNo} и \inlvba{vbQuestion}.
Так как мы не указали третий агрумент, то в заголовке будет выводиться ``Microsoft Excel'' -- имя программы, в которой мы запустили макрос.

\begin{figure}[H]
 \centering
 \includegraphics{pictures/msgbox_02.PNG}
 \caption{сообщение с разными кнопками}
 \label{fig:bas_msgbox_02}
\end{figure}

\par Отдельное внимание обратите на круглые скобки вокруг аргументов \inlvba{MsgBox}.
Их наличие означает, что \inlvba{MsgBox} вызывается как функция, а не как процедура.
Из-за этих скобочек он не только показывает сообщение, но и ещё передаёт в программу, какую кнопку нажал пользователь.
Код нажатой кнопки записывается в переменную \inlvba{return_value}.
В таблице~\ref{tab:bas_msgbox_returns} приведены коды, которые соответствуют разным кнопкам.
Например, если пользователь нажал кнопку ``Yes'', в переменную \inlvba{return_value} запишется значение 6.

\begin{longtable}{lll}
    
    \hline\hline
    Constant & Value & Description  \\
    \hline
    \endhead
    
    vbOK & 1 & OK \\
    vbCancel & 2 & Cancel \\
    vbAbort & 3 & Abort \\
    vbRetry & 4 & Retry \\
    vbIgnore & 5 & Ignore \\
    vbYes & 6 & Yes \\
    vbNo & 7 & No \\

   \hline
   \caption{возращаемые значения Message Box}
   \label{tab:bas_msgbox_returns}
\end{longtable}

\par Если где-то позже в программе мы хотим проверить, какую кнопку нажал пользователь, мы можем сравнивать значение \inlvba{return_value} не с числами 6 и 7, а с константой из таблицы~\ref{tab:bas_msgbox_returns}.
Проверим, какую кнопку нажал пользователь, когда ему был выведен \inlvba{MsgBox} из строки 10.
Для этого мы сравним \inlvba{return_value} с константой из таблицы (\inlvba{vbYes}).
При проверке значения на место константы подставится её код (6), а на место \inlvba{return_value} -- значение, которое в ней записано.
Если оно совпало с 6, то пользователь нажал кнопку ``Yes''.

\par \inlvba{MsgBox} на строке 11 выводит код кнопки, которую нажал пользователь.
В зависимости от того, какую кнопку выбрал пользователь, в сообщение подставляется код 6 или 7 (рис.~\ref{fig:bas_msgbox_03} и~\ref{fig:bas_msgbox_04}).
Чтобы сформировать текст, который будет показывать этот \inlvba{MsgBox}, к куску текста с помощью \inlvba{&} присоединяется код нажатой кнопки из переменной \inlvba{return_value}.

\begin{figure}[H]
 \centering
 \includegraphics{pictures/msgbox_03.PNG}
 \caption{сообщения для кнопок Yes и No}
 \label{fig:bas_msgbox_03}
\end{figure}

\begin{figure}[H]
 \centering
 \includegraphics{pictures/msgbox_04.PNG}
 \caption{сообщения для кнопок Yes и No}
 \label{fig:bas_msgbox_04}
\end{figure}

\subsubsection{Задачки}

\paragraph{Мимикрия}\hfill\\
Создайте такие же \inlvba{TextBox}, как на рис.~\ref{fig:bas_msgbox_06},~\ref{fig:bas_msgbox_07} и~\ref{fig:bas_msgbox_08}. Можно одной процедурой.

\begin{figure}[H]
 \centering
 \includegraphics{pictures/msgbox_06.PNG}
 \caption{сообщения для кнопок Yes и No}
 \label{fig:bas_msgbox_06}
\end{figure}

\begin{figure}[H]
 \centering
 \includegraphics{pictures/msgbox_07.PNG}
 \caption{сообщения для кнопок Yes и No}
 \label{fig:bas_msgbox_07}
\end{figure}

\begin{figure}[H]
 \centering
 \includegraphics{pictures/msgbox_08.PNG}
 \caption{сообщения для кнопок Yes и No}
 \label{fig:bas_msgbox_08}
\end{figure}

\paragraph{Разработка}\hfill\\
Сделайте \inlvba{MsgBox}, в котором спрашивается, облачная ли сейчас погода. Если пользователь отвечает, что да, то показывается новый \inlvba{MsgBox}, в котором сообщается, что это хорошо. В противном случае показывается \inlvba{MsgBox}, в котором сообщается, что это тоже хорошо. У \inlvba{MsgBox} должен быть \inlvba{Title} ``Вопрос о погоде'', и на нём должен быть значок вопроса.